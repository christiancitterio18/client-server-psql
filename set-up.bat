@echo off

echo POSTGRES_USER=pgsql_user >> ./.env
echo POSTGRES_PASSWORD=pgsql_user >> ./.env
echo POSTGRES_DB=pgsql_data >> ./.env
echo [postgresql] >> ./DB/database.ini
echo host=localhost >> ./DB/database.ini
echo database=pgsql_data >> ./DB/database.ini
echo user=pgsql_user >> ./DB/database.ini
echo password=pgsql_user >> ./DB/database.ini

echo Setting things up for tests...
echo
start cmd /C docker-compose up test-db
echo Waiting for db to start up...
echo
timeout /T 60 /NOBREAK
echo "Creating database, on database login password is the same as username..."
echo
psql -U pgsql_user -d pgsql_data -h localhost -p 5432 -f ./DB/db_dump.sql
docker-compose down test-db

echo Setting things up for prod...
echo
start cmd /C docker-compose up db
echo Waiting for db to start up...
echo
timeout /T 60 /NOBREAK
echo "Creating database, on database login password is the same as username..."
echo
psql -U pgsql_user -d pgsql_data -h localhost -p 5432 -f ./DB/db_dump.sql
docker-compose down db