import datetime
import hashlib
from DB import DataManipulation


class ServerMethods(object):
    """
    Implements common server methods
    """
    _AUTH_TABLE: str = 'auth'
    _MESSAGE_TABLE: str = 'message'
    _HISTORY_TABLE: str = 'message_history'
    _USER_TABLE: str = 'users'

    _adminCommands: dict = {
        'info': 'returns server version and created date',
        'uptime': 'returns server life time',
        'addUser': 'add a new user (input data:  username, password, authorization)',
        'changeUser': 'change a user data (input data: user_id, optional[username, password, authorization])',
        'deleteUser': 'delete a user from database (Input data: user_id)',
        'deleteMessage': 'delete a message from database for message moderation (input data: message_id)',
        'getUsers': 'gets list of all users',
        'getMessages': 'gets list of all messages for moderation',
        'getAuths': 'gets list of all authorizations',
        'communicate': 'sends communication for all user (Input data: message text)'
    }

    _userCommands: dict = {
        'message': 'sends a message to a user (Input data: username, message text)',
        'readMessages': 'returns list of messages from a user (Input data: username)',
        'readHistory': 'returns a list of all sent and received messages',
        'cleanInbox': 'cleans the inbox from messages (Deletes all the messages)',
        'changePassword': 'changes password for the current user (Input data: password)',
        'logout': 'log the user out'
    }

    _commands: dict = {
        'help': 'returns a list of available commands with a brief description',
        'login': 'logs in the user (Input data: username, password)',
        'register': 'register a new user (Input data: username, password, repeat password)',
        'quit': 'End the connection'
    }

    # -----------------------------------------------------------------------------------
    # Server standard commands from 1.0
    def __init__(self):
        print('server_info created!')

    def help(self, auth: str = '') -> dict:
        """
        Takes authorization and return list of command base on auth
        :param auth: string
        :return: dict | commands list
        """
        admin_std_out: dict = self._commands
        if auth == 'admin':
            admin_std_out.update(self._userCommands)
            admin_std_out.update(self._adminCommands)
            return admin_std_out
        elif auth == 'user':
            admin_std_out.update(self._userCommands)
            return admin_std_out
        else:
            return admin_std_out

    @staticmethod
    def info(time: datetime) -> dict:
        """
        Returns the current version and the server start date
        :param time: datetime | start time of the server
        :return: dict | server info
        """
        info: dict = {
            'server_version': '2.3',
            'server_created_at': str(time)
        }
        return info

    @staticmethod
    def uptime(time: datetime) -> dict:
        """
        Returns server lifetime
        :param time: datetime | start time of the server
        :return: dict | server life time
        """

        date_now: datetime = datetime.datetime.now()  # takes the datetime now
        res: float = (date_now - time).total_seconds() / 60.00  # calculate difference in minutes
        out: dict  # variable for output

        if res >= 60:
            res = res / 60.00  # calculate difference in hours
            if res >= 24:
                res = res / 24  # calculate difference in days
                out = {
                    'server_life': '{0:2}'.format(res) + 'days'
                }
            else:
                out = {
                    'server_life': '{0:2}'.format(res) + 'hours'
                }
        else:
            out = {
                'server_life': '{0:2}'.format(res) + 'minutes'
            }

        return out

    # --------------------------------------------------------------------------------
    # Authentication related functions

    def login(self, user_name: str, password: str) -> dict:
        """
        Login the user
        :param user_name: str | username
        :param password: str | password
        :return: dict | user info and inbox status
        """
        response: dict = {'user': {}, 'message': ''}
        users: list = DataManipulation.get_data_by_parameters(self._USER_TABLE, username=user_name)
        user: dict
        inbox_len: int

        # check if username is correct
        if len(users) > 0:
            user = users[0]
            inbox_len = self._get_inbox_len(user['id'])

            if user['password'] == hashlib.md5(password.encode('UTF-8')).hexdigest():
                authorization: dict = DataManipulation.get_data(self._AUTH_TABLE, user['auth_id'])
                response['user'] = {
                    'id': user['id'],
                    'username': user['username'],
                    'password': user['password'],
                    'auth': authorization['auth']
                }
                if inbox_len == 5:
                    response['message'] = 'Inbox full, use commands "cleanInbox" to clean your inbox!'
                else:
                    response['message'] = 'you have ' + str(inbox_len) + ' messages in your inbox!\nMax inbox len is 5'
            else:
                response = {'error': 'Failed, password is not correct!'}
        else:
            response = {'error': 'Failed, username is not correct or not exist!'}

        return response

    def register(self, user_name: str, password: str) -> dict:
        """
        Register a new user in the data base
        :param user_name: str | username
        :param password: str | password
        :return: dict | user Info
        """
        user: dict = {'username': user_name,
                      'password': hashlib.md5(password.encode('UTF-8')).hexdigest(),
                      'auth_id': 2}

        response: dict = {'user': {}, 'message': ''}

        # Check if username is already used
        if len(DataManipulation.get_data_by_parameters(self._USER_TABLE, username=user_name)) > 0:
            response = {'error': 'Failed, username is already used!'}
        else:
            if DataManipulation.add_data(self._USER_TABLE, user):
                user = DataManipulation.get_data_by_parameters(self._USER_TABLE, username=user_name)[0]
                authorization: dict = DataManipulation.get_data(self._AUTH_TABLE, user['auth_id'])
                response['user'] = {
                    'id': user['id'],
                    'username': user['username'],
                    'password': user['password'],
                    'auth': authorization['auth']
                }
                response['message'] = 'Welcome new User! Your Inbox max len is 5'
            else:
                response = {'error': 'Failed, unknown error!'}

        return response

    # ------------------------------------------------------------------------
    # Message related functions

    def get_messages(self) -> list:
        """
        Get the list of all messages for moderation
        :return: list[dict] | list of all users
        """
        return DataManipulation.get_all(self._MESSAGE_TABLE)

    def read_messages(self, user: str, uid: int) -> dict:
        """
        Reads all the messages from a user
        :param user: str | sender username
        :param uid: int | current user
        :return: dict | list of all received messages from user
        """
        response: dict = {'messages': []}
        user_id: dict = DataManipulation.get_data_by_parameters(self._USER_TABLE, username=user)[0]
        history_inbound: list = DataManipulation.get_data_by_parameters(self._HISTORY_TABLE, receiver_id=uid)
        history_outbound: list = DataManipulation.get_data_by_parameters(self._MESSAGE_TABLE, sender_id=user_id['id'])
        for x in history_outbound:
            for y in history_inbound:
                if x['id'] == y['message_id']:
                    response['messages'].append(x)

        return response

    def read_history(self, uid: int) -> dict:
        """
        Returns all the received and sent messages
        :param uid: int | current user
        :return: dict | list of all received and sent messages
        """
        response: dict = {'inbound': [], 'outbound': []}
        received: list = DataManipulation.get_data_by_parameters(self._HISTORY_TABLE, receiver_id=uid)
        sent: list = DataManipulation.get_data_by_parameters(self._MESSAGE_TABLE, sender_id=uid)
        for message_received in received:
            message: dict = DataManipulation.get_data(self._MESSAGE_TABLE, message_received['message_id'])
            user: dict = DataManipulation.get_data(self._USER_TABLE, message['sender_id'])
            message['received_from'] = user['username']
            response['inbound'].append(message)

        for message_sent in sent:
            message_history: dict = DataManipulation.get_data_by_parameters(self._HISTORY_TABLE,
                                                                            message_id=message_sent['id'])[0]
            user: dict = DataManipulation.get_data(self._USER_TABLE, message_history['receiver_id'])
            message_sent['received_from'] = user['username']
            response['outbound'].append(message_sent)

        return response

    def message(self, user: str, text: str, uid: int) -> dict:
        """
        Creates and sends a message to user
        :param user: str | receiver username
        :param text: str | message text (max 255 char)
        :param uid: int | current user
        :return: dict | response message
        """
        response: dict = {'message': ''}
        users: list = DataManipulation.get_data_by_parameters(self._USER_TABLE, username=user)
        receiver: dict

        if len(users) > 0:
            receiver = users[0]
        else:
            response = {'error': 'Failed, user don\'t exists'}
            return response

        if self._get_inbox_len(receiver['id']) >= 5 and int(receiver['auth_id']) != 1:
            response = {'error': 'Receiver inbox is full, message cannot be sent'}
            return response

        if len(text) <= 255:
            message: dict = {'text': text, 'sender_id': str(uid)}
            if DataManipulation.add_data(self._MESSAGE_TABLE, message):
                message_id: int = DataManipulation.get_data_by_parameters(self._MESSAGE_TABLE, text=text)[-1]['id']
                if self.add_history_element(message_id, receiver['id']):
                    response['message'] = 'Message successfully sent!'
                else:
                    response = {'error': 'Failed, message was not sent!'}
                    DataManipulation.delete_data(self._MESSAGE_TABLE, message_id)
            else:
                response = {'error': 'Failed, message was not sent!'}
        else:
            response = {'error': 'Failed, text is to long,  max length is 255'}

        return response

    def communicate(self, text: str, uid: int) -> dict:
        """
        Creates and sends a message to all users
        :param text: str | message text (max 255 char)
        :param uid: int | current user
        :return: dict | result message
        """
        response: dict = {'message': '', 'unsent_to_users': []}
        receivers: list = self.get_users()

        if len(text) <= 255:
            message: dict = {'text': text, 'sender_id': uid}

            if DataManipulation.add_data(self._MESSAGE_TABLE, message):
                message_id: int = DataManipulation.get_data_by_parameters(self._MESSAGE_TABLE, text=text)[0]['id']

                for x in receivers:
                    inbox: int = self._get_inbox_len(x['id'])
                    if (not self.add_history_element(message_id, x['id'])) or (inbox >= 5 and x['auth_id'] != 1):
                        response['unsent_to_users'].append(x['username'])
                response['message'] = 'Message successfully sent!'

            else:
                response = {'error': 'Failed, message was not sent!'}
        else:
            response = {'error': 'Failed, text is to long,  max length is 255!'}

        return response

    def delete_message(self, message_id: int) -> dict:
        """
        Deletes an existing message
        :param message_id: int | message to delete
        :return: dict | result of the operation
        """
        response: dict = {'message': ''}
        is_history: bool = self.delete_from_history(message_id=message_id)
        if is_history:
            if DataManipulation.delete_data(self._MESSAGE_TABLE, message_id):
                response['message'] = 'Message successfully Deleted!'
            else:
                response = {'error': 'Failed to delete message, history deleted!'}
        else:
            response = {'error': 'Failed to delete Message!'}

        return response

    def clean_inbox(self, uid: int) -> dict:
        """
        Deletes all the messages in user Inbox
        :param uid: int | current user
        :return: boolean | result of the operation
        """
        response: dict = {'message': ''}

        if DataManipulation.delete_by_parameters(self._HISTORY_TABLE, receiver_id=uid):
            response['message'] = 'Inbox successfully cleaned!'
        else:
            response = {'error': 'Failed, error unknown!'}

        return response

    # ----------------------------------------------------------------

    # User related functions

    def get_users(self) -> list:
        """
        Get the list of all users
        :return: list[dict] | list of all users
        """
        return DataManipulation.get_all(self._USER_TABLE)

    def add_user(self, username: str, password: str, auth: str) -> dict:
        """
        Add a new user to the data base
        :param username: str | username
        :param password: str | password
        :param auth: str | user authorization
        :return: dict | result of the operation
        """
        response: dict = {'message': ''}
        auth: dict = DataManipulation.get_data_by_parameters(self._AUTH_TABLE, auth=auth)[0]
        user: dict = {'username': username,
                      'password': hashlib.md5(password.encode('UTF-8')).hexdigest(),
                      'auth_id': auth['id']}

        if len(DataManipulation.get_data_by_parameters(self._USER_TABLE, username=username)) > 0:
            response = {'error': 'Failed, username is already used!'}
        else:
            if DataManipulation.add_data(self._USER_TABLE, user):
                response['message'] = 'User successfully added!'
            else:
                response = {'error': 'Failed to add user!'}

        return response

    def delete_user(self, uid: int) -> dict:
        """
        Delete a user from the data base, and his inbox
        :param uid: int | user to delete
        :return: boolean |result of the operation
        """
        response: dict = {'message': ''}
        if self.delete_from_history(user_id=uid):
            if DataManipulation.delete_data(self._USER_TABLE, uid):
                response['message'] = 'User successfully Deleted!'
            else:
                response = {'error': 'Failed to delete user!'}
        else:
            response = {'error': 'Failed to delete user!'}

        return response

    def change_user(self, user_id: int, username: str = '', password: str = '', auth: str = '') -> dict:
        """
        Modify user data
        :param user_id: int | user id
        :param username: str | username
        :param password: str | password
        :param auth: str | authorization
        :return: dict | result message
        """
        response: dict = {'message': ''}
        user: dict = DataManipulation.get_data(self._USER_TABLE, object_id=user_id)

        if len(username) > 0:
            user['username'] = username

        if len(password) > 0:
            user['password'] = hashlib.md5(password.encode('UTF-8')).hexdigest()

        if len(auth) > 0:
            auth: dict = DataManipulation.get_data_by_parameters(self._AUTH_TABLE, auth=auth)[0]
            user['auth_id'] = auth['id']

        user_data = {'username': user['username'], 'password': user['password'], 'auth_id': user['auth_id']}

        if DataManipulation.modify_data(self._USER_TABLE, user_id, user_data):
            response['message'] = 'User data successfully changed!'
            return response
        else:
            response = {'error': 'Failed to change user data!'}

        return response

    # ----------------------------------------------------------------------------

    # message history related functions
    def add_history_element(self, message_id: int, user_id: int) -> bool:
        """
        Adds a new message history object
        :param message_id: int | message id
        :param user_id: int | receiver or sender id
        :return: bool | result of the operation
        """
        new_data: dict = {'message_id': message_id, 'receiver_id': user_id}
        return DataManipulation.add_data(self._HISTORY_TABLE, new_data)

    def delete_from_history(self, message_id: int = 0, user_id: int = 0) -> bool:
        """
        Deletes history info
        :param message_id: int |  message id to delete
        :param user_id: int | user_id (optional)
        :return: bool | result of the operation
        """
        if int(message_id) > 0:
            if int(user_id) > 0:
                return DataManipulation.delete_by_parameters(self._HISTORY_TABLE, message_id=message_id,
                                                             receiver_id=user_id)

            return DataManipulation.delete_by_parameters(self._HISTORY_TABLE, message_id=message_id)
        else:
            if int(user_id) > 0:
                return DataManipulation.delete_by_parameters(self._HISTORY_TABLE, receiver_id=user_id)
            return True

    # -------------------------------------------------------------------------

    # auth related functions
    def get_auths(self) -> list:
        """
        Gets a list of authorizations
        :return: list | list of authorizations
        """
        return DataManipulation.get_all(self._AUTH_TABLE)

    # ----------------------------------------------------------------------------

    # Helper methods
    def _get_inbox_len(self, user_id: int) -> int:
        """
        Gets the inbox length of an user
        :param user_id: int | the user id
        :return: int | length of the user inbox
        """
        history: list = DataManipulation.get_data_by_parameters(self._HISTORY_TABLE, receiver_id=user_id)
        inbox: list = [x for x in history]
        return len(inbox)
