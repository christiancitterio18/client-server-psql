
class ClientCommands:

    @staticmethod
    def prepare_command_user(command: str, **values):
        command: dict = {
                'command': command,
                'uid': 1,
                'auth': 'user',
                'data': values
        }
        return command

    @staticmethod
    def prepare_command_admin(command: str, **values):
        command: dict = {
                'command': command,
                'uid': 1,
                'auth': 'admin',
                'data': values
        }
        return command

    @staticmethod
    def prepare_command(command: str, **values):
        command: dict = {
                'command': command,
                'uid': '',
                'auth': '',
                'data': values
        }
        return command
