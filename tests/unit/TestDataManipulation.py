import hashlib
import unittest
from DB import DataManipulation


class TestDataManipulation(unittest.TestCase):

    PATH: str = 'users'

    def test_data_manipulation_get_all(self):
        """
        Test for data get of all table
        """
        #  Test for data id prop
        self.assertIsInstance(DataManipulation.get_all(self.PATH), list, 'Should be a list!')
        self.assertGreaterEqual(len(DataManipulation.get_all(self.PATH)), 1,
                                'Should be a list with len >= 1!')
        self.assertIsInstance(DataManipulation.get_all(self.PATH)[0], dict, 'Should be a dict!')
        self.assertEqual(len(DataManipulation.get_all(self.PATH)[0]), 4, 'Should have 4 properties!')
        self.assertRaises(TypeError, DataManipulation.get_all)

    def test_data_manipulation_get_data(self):
        """
        Test for a single data object get
        """
        #  Test for len of the data and object properties
        self.assertIsInstance(DataManipulation.get_data(self.PATH, 1), dict, 'Should be a dict!')
        self.assertEqual(len(DataManipulation.get_data(self.PATH, 1)), 4, 'Should have 4 properties!')
        self.assertTrue('id' in DataManipulation.get_data(self.PATH, 1).keys(), 'Should have an id!')
        self.assertTrue('username' in DataManipulation.get_data(self.PATH, 1).keys(), 'Should have a username!')
        self.assertTrue('password' in DataManipulation.get_data(self.PATH, 1).keys(), 'Should have a password!')
        self.assertTrue('auth_id' in DataManipulation.get_data(self.PATH, 1).keys(), 'Should have ann auth_id!')
        # Test for errors
        self.assertRaises(TypeError, DataManipulation.get_data, self.PATH)
        self.assertRaises(TypeError, DataManipulation.get_data)

    def test_data_manipulation_get_data_by_parameters(self):
        """
        Tests for data object get by parameters
        """
        data_assertion = DataManipulation.get_data_by_parameters(self.PATH, username='admin')
        #  Test for len of the data and object properties
        self.assertIsInstance(data_assertion, list, 'Should be a list!')
        self.assertIsInstance(data_assertion[0], dict, 'Should be a dict!')
        self.assertEqual(len(data_assertion[0]), 4, 'Should have 4 properties!')
        self.assertTrue('id' in data_assertion[0].keys(), 'Should have an id!')
        self.assertTrue('username' in data_assertion[0].keys(), 'Should have a username!')
        self.assertTrue('password' in data_assertion[0].keys(), 'Should have a password!')
        self.assertTrue('auth_id' in data_assertion[0].keys(), 'Should have ann auth_id!')
        # Test for errors
        self.assertRaises(TypeError, DataManipulation.get_data)

    def test_data_manipulation_modify_data(self):
        """
        Tests for data object modification
        """
        #  Tests a data object modification

        comparison_data: dict = DataManipulation.get_data(self.PATH, 1)
        self.assertTrue(DataManipulation.modify_data(self.PATH, 1, {
                        'username': 'new',
                        'password': hashlib.md5('new'.encode('UTF-8')).hexdigest(),
                        'auth_id': 1,
                        }), 'Should return True!')
        self.assertNotEqual(DataManipulation.get_data(self.PATH, 1)['username'], comparison_data['username'],
                            'Should be not equal!')

        comparison_data = DataManipulation.get_data(self.PATH, 1)
        self.assertTrue(DataManipulation.modify_data(self.PATH, 1, {
                        'username': 'admin',
                        'password': hashlib.md5('admin'.encode('UTF-8')).hexdigest(),
                        'auth_id': 1}), "Should return True!")
        self.assertNotEqual(DataManipulation.get_data(self.PATH, 1)['password'], comparison_data['password'],
                            'Should be not equal')
        self.assertEqual(len(DataManipulation.get_data(self.PATH, 1)['password']), 32, 'Password len should be 32!')
        #  Tests for errors
        self.assertRaises(TypeError, DataManipulation.modify_data, {'username': 'test'})

    def test_data_manipulation_add_data(self):
        """
        Tests for adding a data object
        """
        len_comparison: int = len(DataManipulation.get_all(self.PATH))
        test_data: dict = {'username': 'tester',
                           'password': hashlib.md5('pass'.encode('UTF-8')).hexdigest(), 'auth_id': 1}
        # Tests adding a data object
        self.assertTrue(DataManipulation.add_data(self.PATH, test_data), 'Should be True!')
        self.assertTrue('id' in DataManipulation.get_data(self.PATH, len_comparison + 1), 'Should have an id!')
        self.assertTrue('username' in DataManipulation.get_data(self.PATH, len_comparison + 1),
                        'Should have a username!')
        self.assertTrue('password' in DataManipulation.get_data(self.PATH, len_comparison + 1),
                        'Should have a password!')
        self.assertTrue('auth_id' in DataManipulation.get_data(self.PATH, len_comparison + 1),
                        'Should have an auth_id!')
        # Tests for errors
        self.assertRaises(TypeError, DataManipulation.add_data, self.PATH)
        self.assertRaises(TypeError, DataManipulation.add_data)

    def test_data_manipulation_delete_data(self):
        """
        Tests for data object deletion
        """
        users: list = DataManipulation.get_all(self.PATH)
        len_comparison: int = len(users)
        # Tests for data object deletion
        self.assertTrue(DataManipulation.delete_data(self.PATH, users[-1]['id']), 'Should return True!')
        self.assertGreater(len_comparison, len(DataManipulation.get_all(self.PATH)),
                           'New len should be lesser tha previous')
        self.assertGreater(len(DataManipulation.get_all(self.PATH)), 0, 'Data len should be greather than 0!')
        # Tests for errors
        self.assertRaises(TypeError, DataManipulation.delete_data)
        self.assertRaises(TypeError, DataManipulation.delete_data, self.PATH)

    def test_data_manipulation_delete_by_parameters(self):
        """
        Tests for data object deletion by parameters
        """
        DataManipulation.add_data(self.PATH, {'username': 'test2',
                                              'password': hashlib.md5('pass'.encode('UTF-8')).hexdigest(),
                                              'auth_id': 2})
        len_comparison = len(DataManipulation.get_all(self.PATH))
        # Tests for data object deletion
        self.assertTrue(DataManipulation.delete_by_parameters(self.PATH, auth_id=2), 'Should return True!')
        self.assertGreater(len_comparison, len(DataManipulation.get_all(self.PATH)),
                           'New len should be lesser tha previous')
        self.assertGreater(len(DataManipulation.get_all(self.PATH)), 0, 'Data len should be greather than 0!')
        # Tests for errors
        self.assertRaises(TypeError, DataManipulation.delete_by_parameters)


if __name__ == '__main__':
    unittest.main()
