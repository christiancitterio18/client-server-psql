import unittest
from src.ClientMethods import ClientMethods


class TestClientMethod(unittest.TestCase):

    def test_prepare_data(self):
        """
        Tests client methods with user data
        """
        assertion_data = ClientMethods.prepare_data('help', 1, 'Administrator')

        self.assertTrue(isinstance(assertion_data, dict), 'Should be a dictionary!')
        self.assertGreater(len(assertion_data['command']), 0, 'Should be a command!')
        self.assertIsInstance(assertion_data['uid'], int)
        self.assertGreater(assertion_data['uid'], 0, 'Should be grater than 0!')
        self.assertGreater(len(assertion_data['auth']), 0, 'Should be an authorization name!')
        self.assertGreater(len(assertion_data), 3, 'Should be greater than 3!')
        self.assertEqual(assertion_data['data'], {}, 'Should be empty!')

    def test_prepare_data_with_values_dict(self):
        """
        Tests client method with required data and values dict
        """
        assertion_data = ClientMethods.prepare_data('command', username='test', password='test')

        self.assertTrue(isinstance(assertion_data, dict), 'Should be a dictionary!')
        self.assertGreater(len(assertion_data), 3, 'Should be greater than 3!')
        self.assertGreater(len(assertion_data['command']), 0, 'Should be a command!')
        self.assertEqual(len(assertion_data['uid']), 0, 'Should be an empty String!')
        self.assertEqual(len(assertion_data['auth']), 0, 'Should be an empty String!')
        self.assertTrue(isinstance(assertion_data['data'], dict), 'Should be a dictionary!')
        self.assertEqual(len(assertion_data['data']), 2, 'Should be 2 elements!')

    def test_prepare_data_exceptions(self):
        """
        Tests if lack of required data send error messages
        """
        self.assertRaises(TypeError, ClientMethods.prepare_data, uid=1, auth='Admin')
        self.assertRaises(TypeError, ClientMethods.prepare_data)


if __name__ == '__main__':
    unittest.main()
