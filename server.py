#!/usr/bin/env python3
import socket
import datetime
from src.ServerMethods import ServerMethods
import json

PORT: int = 8081
ADDRESS: str = '127.0.0.1'
DATE: datetime = datetime.datetime.now()
_SERVER_INFO: ServerMethods = ServerMethods()

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((ADDRESS, PORT))
    s.listen()
    conn, address = s.accept()
    with conn:
        print('Connected by', address)

        # server main loop
        while True:
            data_in: bytes = conn.recv(1024)
            data_out: str = ''

            command_data: dict = json.loads(data_in)
            command: str = command_data['command']
            uid: int = command_data['uid']
            auth: str = command_data['auth']
            data: dict = command_data['data']

            if command == 'quit':
                conn.close()
                break

            if auth == 'admin':

                if command == 'help':
                    response = _SERVER_INFO.help(auth)
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'message':
                    response = _SERVER_INFO.message(data['user'], data['text'], uid)
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'readMessages':
                    response = _SERVER_INFO.read_messages(data['user'], uid)
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'readHistory':
                    response = _SERVER_INFO.read_history(uid)
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'cleanInbox':
                    response = _SERVER_INFO.clean_inbox(uid=uid)
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'changePassword':
                    response = _SERVER_INFO.change_user(user_id=uid,
                                                        password=data['password'],)
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'info':
                    response = _SERVER_INFO.info(DATE)
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'uptime':
                    response = _SERVER_INFO.uptime(DATE)
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'addUser':
                    response = _SERVER_INFO.add_user(data['username'], data['password'], data['authorization'])
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'addAuth':
                    response = _SERVER_INFO.add_auth(data['authorization'])
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'addType':
                    response = _SERVER_INFO.add_type(data['type'])
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'changeUser':
                    response = _SERVER_INFO.change_user(user_id=data['user_id'], username=data['username'],
                                                        password=data['password'], auth=data['authorization'])
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'deleteUser':
                    response = _SERVER_INFO.delete_user(data['user_id'])
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'deleteMessage':
                    response = _SERVER_INFO.delete_message(data['message_id'])
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'getUsers':
                    response = _SERVER_INFO.get_users()
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'getMessages':
                    response = _SERVER_INFO.get_messages()
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'getAuths':
                    response = _SERVER_INFO.get_auths()
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'communicate':
                    response = _SERVER_INFO.communicate(data['text'], uid)
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'logout':
                    response = {'message': 'See You Soon!' }
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                else:
                    response = {'error': '404, command not found!'}
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

            elif auth == 'user':
                if command == 'help':
                    response = _SERVER_INFO.help(auth)
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'message':
                    response = _SERVER_INFO.message(data['user'], data['text'], uid)
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'readMessages':
                    response = _SERVER_INFO.read_messages(data['user'], uid)
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'readHistory':
                    response = _SERVER_INFO.read_history(uid)
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'cleanInbox':
                    response = _SERVER_INFO.clean_inbox(uid)
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'changePassword':
                    response = _SERVER_INFO.change_user(user_id=uid,
                                                        password=data['password'],)
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'logout':
                    response = {'message': 'See You Soon!'}
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                else:
                    response = {'error': '404, command not found!'}
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

            else:

                if command == 'help':
                    response = _SERVER_INFO.help(auth)
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'login':
                    response = _SERVER_INFO.login(data['username'], data['password'])
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))

                elif command == 'register':

                    if data['password'] == data['repeat_password']:
                        response = _SERVER_INFO.register(data['username'], data['password'])
                        data_out = json.dumps(response, indent=2)
                        conn.sendall(bytes(data_out.encode()))

                    else:
                        response = {'error': 'Passwords don\'t match up!'}
                        data_out = json.dumps(response, indent=2)
                        conn.sendall(bytes(data_out.encode()))

                else:
                    response = {'error': '404, command not found!'}
                    data_out = json.dumps(response, indent=2)
                    conn.sendall(bytes(data_out.encode()))
