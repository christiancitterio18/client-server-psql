import sys

from DB import connector


def get_all(data_table: str) -> list:
    """
    Returns a list of data
    :param data_table: str | data table to get objects from DB
    :return: list | list of data
    """
    conn, cur = connector.connect()
    data = None

    try:
        cur.execute('SELECT * FROM {}'.format(data_table))
        data = cur.fetchall()
        cur.close()
        conn.close()
    except Exception as e:
        print(f'An error occurred: {e.with_traceback(sys.exc_info()[2])}')

    return transform_data(data_table, data)


def get_data(data_table: str, object_id: int) -> dict:
    """
    Returns a single data record
    :param data_table: str | data table to get objects from DB
    :param object_id: int | data id
    :return: dict | data representation as python dictionary
    """
    conn, cur = connector.connect()
    data = None

    try:
        cur.execute(f'SELECT * FROM {data_table} WHERE id = {object_id}')
        data = cur.fetchall()
        cur.close()
        conn.close()
    except Exception as e:
        print(f'An error occurred: {e.with_traceback(sys.exc_info()[2])}')

    return transform_data(data_table, data)[0]


def modify_data(data_table: str, object_id: int, new_data: dict) -> bool:
    """
    Modifies data objects
    :param data_table: str | data file data_table
    :param object_id: int | data id to be modified
    :param new_data: dict | the new object data
    :return: bool | operation result
    """
    conn, cur = connector.connect()
    data_query: list = []

    for k, v in new_data.items():
        if 'id' in k:
            data_query.append(k + ' = ' + str(v))
        else:
            data_query.append(k + ' = \'' + v + '\'')

    try:
        cur.execute(f'UPDATE {data_table} SET ' + ', '.join(data_query)
                     + f' WHERE id = {object_id};')

        conn.commit()
        cur.close()
        conn.close()
        return True
    except Exception as e:
        print(f'An error occurred: {e.with_traceback(sys.exc_info()[2])}')

    return False


def add_data(data_table: str, new_data: dict) -> bool:
    """
    Adds a new object in the data file
    :param data_table: str | data file data_table
    :param new_data: dict | new data to be added
    :return: bool | operation result
    """
    conn, cur = connector.connect()
    columns: list = []
    data: list = []

    for k, v in new_data.items():
        if 'id' in k:
            columns.append(k)
            data.append(str(v))
        else:
            columns.append(k)
            data.append(f'\'{v}\'')

    try:
        cur.execute(f'INSERT INTO {data_table} (' + ', '.join(columns)
                    + ') VALUES (' + ', '.join(data) + ');')
        conn.commit()
        cur.close()
        conn.close()
        return True
    except Exception as e:
        print(f'An error occurred: {e.with_traceback(sys.exc_info()[2])}')

    return False


def delete_data(data_table: str, object_id: int) -> bool:
    """
    Deletes object data
    :param data_table: str | data file data_table
    :param object_id: int | data id to be deleted
    :return: bool | operation result
    """
    conn, cur = connector.connect()
    try:
        cur.execute(f'DELETE FROM {data_table} WHERE id = {str(object_id)}')
        conn.commit()
        cur.close()
        conn.close()
        return True
    except Exception as e:
        print(f'An error occurred: {e.with_traceback(sys.exc_info()[2])}')

    return False

# ---------------------------------------------------------------

# custom methods


def get_data_by_parameters(data_table: str, **values) -> list:
    """
    Returns a list of data object
    :param data_table: str | data file data_table
    :param values: dict | parameters to match data
    :return: list | list of data
    """
    conn, cur = connector.connect()
    data_query: list = []
    response: list = None

    for k, v in values.items():
        if 'id' in k:
            data_query.append(k + ' = ' + str(v))
        else:
            data_query.append(k + ' = \'' + v + '\'')

    try:
        cur.execute(f'SELECT * FROM {data_table} WHERE ' + ' AND '.join(data_query))
        response = cur.fetchall()
        cur.close()
        conn.close()
    except Exception as e:
        print(f'An error occurred: {e.with_traceback(sys.exc_info()[2])}')

    print(response)
    return transform_data(data_table, response)


def delete_by_parameters(data_table, **values) -> bool:
    """
    Deletes data by given parameters
    :param data_table: str | data file data_table
    :param values: dict | parameters to match data
    :return: bool | operation result
    """
    conn, cur = connector.connect()
    data_query: list = []

    for k, v in values.items():
        if 'id' in k:
            data_query.append(k + ' = ' + str(v))
        else:
            data_query.append(k + ' = "' + v + '"')

    try:
        cur.execute(f'DELETE FROM {data_table} WHERE ' + ' AND '.join(data_query))
        conn.commit()
        cur.close()
        conn.close()
        return True
    except Exception as e:
        print(f'An error occurred: {e.with_traceback(sys.exc_info()[2])}')

    return False


def transform_data(data_table: str, data: list) -> list:
    result: list = []

    if data_table == 'users':
        for bit in data:
            result_data: dict = {'id': bit[0], 'username': bit[1], 'password': bit[2], 'auth_id': bit[3]}
            result.append(result_data)
    elif data_table == 'message':
        for bit in data:
            result_data: dict = {'id': bit[0], 'text': bit[1], 'sender_id': bit[2]}
            result.append(result_data)
    elif data_table == 'message_history':
        for bit in data:
            result_data: dict = {'message_id': bit[0], 'receiver_id': bit[1]}
            result.append(result_data)
    elif data_table == 'auth':
        for bit in data:
            result_data: dict = {'id': bit[0], 'auth': bit[1]}
            result.append(result_data)
    else:
        return None

    return result

