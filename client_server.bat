@echo off

echo Setting things up...
echo
docker-compose up -d db

echo Starting client and server...
echo
start cmd /C python server.py
echo Please enter a command...
python client.py

echo Cleaning up...
docker-compose down db